// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/**
1、数组(Array)
*/

//1、创建数组
var someInts = [Int]()
var threeDoubles = [Double](count: 3, repeatedValue: 0.0)
var anotherThreeDoubles = [Double](count: 3, repeatedValue: 2.5)
var sixDoubles = threeDoubles + anotherThreeDoubles

var shoppingList = ["Eggs","Milk"];

//2、访问数组
print("the shopping list contain \(shoppingList.count) items")

if shoppingList.isEmpty{
    print("the shopping list is empty")
}else{
    print("the shopping list is not empty")
}

print("the first item is \(shoppingList[0])")

//3、数组修改元素
shoppingList[0] = "Six eggs"

//4、数组添加元素
shoppingList.append("Flour")

shoppingList += ["baking powder"]

shoppingList += ["Chocolate Spread", "Cheese", "Butter"]

shoppingList[4...6] = ["Bananas","Apples"]

shoppingList.insert("Maple Syrup", atIndex: 0)

//5、数组删除元素
let mapleSyrup = shoppingList.removeAtIndex(0)

let apples = shoppingList.removeLast()

//6、数组的遍历
for item in shoppingList{
    print(item)
}

//print is :
// Six eggs
// Milk
// Flour
// Baking Powder
// Bananas

//7、使用全局enumerate 函数来进行数组的遍历
for (index,value) in shoppingList.enumerate(){
    print("Item \(index + 1):\(value)")
}
//print is:
// Item 1: Six eggs
// Item 2: Milk
// Item 3: Flour
// Item 4: Baking Powder
// Item 5: Bananas




/**
2、字典(Dictionary)
**/

//1、创建字典
var nameOfInteger = [Int :String]()
nameOfInteger = [:];

var airports = ["YYZ":"Toronto Pearson","DUB":"Dublin"]

//2、访问字典
print("the airports dictionary contains \(airports.count) items")

if airports.isEmpty{
    print("empty")
}else{
    print("not empty")
}

let yyzAirport = airports["YYZ"]
print("YYZ is \(yyzAirport)")

//3、字典新增修改元素
airports["LHR"] = "London"
airports["LHR"] = "London Heathrow"

//updateValue方法返回修改前的值，因为原值可能不存在，所以返回的是一个可选类型
if let oldValue = airports.updateValue("Dublin Airport", forKey: "DUB"){
    print("the old value for DUB was \(oldValue)")
}
//4、字典删除元素
airports["APL"] = "Apple International"
airports["APL"] = nil

//对于removeValueForKey方法，如果键值存在，返回被移除的value值，如果不存在，返回nil
if let removedValue = airports.removeValueForKey("DUB"){
    print("the renmoved airport's name is \(removedValue)")
}else{
    print("the airports dictionary dose not contain a value for DUB")
}

//5、字典遍历
for (airportCode, airportName) in airports{
    print("\(airportCode):\(airportName)")
}

for airportCode in airports.keys{
    print("airport code:\(airportCode)")
}

for airportName in airports.values{
    print("airport name:\(airportName)")
}

let airportCodes = [String](airports.keys)
let airportNames = [String](airports.values);
