// Playground - noun: a place where people can play

import UIKit

/**
1、For循环
**/

// 使用 for-in 进行遍历
for index in 1...5 {
    print("\(index) times 5 is \(index * 5)")
}

let names = ["Anna", "Alex", "Brian", "Jack"]
for name in names {
    print("Hello, \(name)!")
}

let numberOfLegs = ["spider": 8, "ant": 6, "cat": 4]
for (animalName, legCount) in numberOfLegs {
    print("\(animalName)s have \(legCount) legs")
}

for character in "Hello".characters {
    print("哈哈\(character)")
}

let base = 3
let power = 10
var answer = 1

for _ in 1...power{
    answer *= base
}

//使用for条件递增进行遍历
for var index = 0;index < 3; index++ {
    print("index is \(index)")
}

//2、while循环
var countOne = 3
while countOne < 2{
    countOne += 3
}

print("this countOne is now \(countOne)")

var countTwo = 3
repeat{
    countTwo += 3
}while countTwo < 2

print("the countTwo is now \(countTwo)")

//3、if条件语句
let temperatureInFahrenheit = 40
if temperatureInFahrenheit <= 32{
    print("it's very old. Consider wearing a scarf'")
}else if temperatureInFahrenheit >= 86{
    print("it's really warm.Don't forget to wear sunscreen'")
}else{
    print("it's not that cold.wear t_shirt'")
}


//4、switch条件语句
//在 Swift 中，当匹配的 case 块中的代码执行完毕后，程序会终止 switch 语句，而不会继续执行下一个 case 块，不需要在 case 块中显式地使用 break 语句
let someCharacter:Character = "e"
switch someCharacter{
    case "a","e","i","o","u" :
        print("\(someCharacter) is a vowel")
    default:
        print("\(someCharacter) is a vowel")
}

//1、 switch的范围匹配
let age = 45
switch age {
case 0...5 :
    print("it's a baby")
case 6...18 :
    print("it's a teenager")
case 19...55 :
    print("it's an adult")
default:
    print("it' an old people")
}

//2、switch中的元组
//switch 的元组中的元素可以是值，也可以是范围，也可使用下划线（_）来匹配所有可能的值。
let somePoint = (1, 1)
switch somePoint {
case (0, 0):
    print("(0, 0) is at the origin")
case (_, 0):
    print("(\(somePoint.0), 0) is on the x-axis")
case (0, _):
    print("(0, \(somePoint.1)) is on the y-axis")
case (-2...2, -2...2):
    print("(\(somePoint.0), \(somePoint.1)) is inside the box")
default:
    print("(\(somePoint.0), \(somePoint.1)) is outside of the box")
}
//3、switch值绑定(value Bindings)
//case 块允许将匹配的值绑定到一个临时的常量或变量上，在对应的 case 块里就可以引用这个常量或者变量
let anotherPoint = (2, 0)
switch anotherPoint {
case (let x, 0):
    print("on the x-axis with an x value of \(x)")
case (0, let y):
    print("on the y-axis with a y value of \(y)")
case let (x, y):
    print("somewhere else at (\(x), \(y))")
}
// prints "on the x-axis with an x value of 2"

//4、switch中的where语句
let yetAnotherPoint = (1, -1)
switch yetAnotherPoint {
case let (x, y) where x == y:
    print("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    print("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    print("(\(x), \(y)) is just some arbitrary point")
}
// prints "(1, -1) is on the line x == -y"



//5、控制转移语句
//Swift 有四种控制转移语句：continue、break、fallthrough、return。
//(1)continue
//告诉循环体停止本次循环迭代，重新开始下次循环迭代。
// 找出1-50中既是6的倍数又是7的倍数的数
for(var i = 1; i < 50; i++){
    if(i % 6 != 0) {
        continue
    }
    if(i % 7 == 0) {
        print("\(i) is a multiple of 6 and 7")
    }
}
// prints "42 is a multiple of 6 and 7"

//(2)break
//中断循环体的执行
//当一个 switch 分支仅仅包含注释时,会被报编译时错误。注释不是代码语句而且也 不能让 switch 分支达到被忽略的效果。你总是可以使用 break 来忽略某个分支。

// 判断单词中是否包含字母L
let letter = "HELLO"
var isLetterContainL = false
for character in letter.characters {
    if(character == "L") {
        isLetterContainL = true
        break
    }
}

//(3)fallthrough
let integerToDescribe = 5
var description = "The number \(integerToDescribe) is"
switch integerToDescribe {
case 2, 3, 5, 7, 11, 13, 17, 19:
    description += " a prime number, and also"
    fallthrough
default:
    description += " an integer."
}
print(description)
// prints "The number 5 is a prime number, and also an integer."

//(4)return  函数体结束

//(5)标签语句
//使用标签来标记一个循环体或者 switch 代码块，当使用 break 或者 continue 时，带上这个标签，可以控制该标签代表对象的中断或者执行

//eg:检查数组（该数组元素限定0-50之间的整数）中是否包含1到20之间的质数
var numbers = [6,23,17,20];
myLoop:for number in numbers{
    switch number{
    case 20...50:
        continue myLoop
    case 1,2,3,5,7,11,13,17,19:
        print("number has a prime of \(number)")
        break myLoop
    default:
        break
    }
}

let label = "the width is"
let width = 76
let labelwidth = label + String(width)
let widthLabel = label + "\(width)"


//let emptyArray = String[]()

let emptyDictionary = Dictionary<String,Float>()


let shoppingList = []


let shoplist = [:]



