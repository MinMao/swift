// Playground - noun: a place where people can play

import Cocoa

//访问控制(Access Control)
/***
1、模块和源文件
Swift 中是基于模块和源文件进行访问控制的。模块指 Framework 或 Application，可以通过 import 引用。源文件指 Swift File。
通常一个模块可以包含一个或多个源文件，一个源文件包含一个类，类中包含函数、属性等类型。

2、访问级别
为了方便描述，将可以设置访问级别的属性、函数、基本类型等称之为“实体”。
三种访问级别：
public。自己或者别人通过引入模块，可以访问模块中源文件里的任何实体。通常情况下，某个接口或 Framework 是可以被任何人使用的，你可以将其设置为 public 级别。
internal。自己可以访问模块中源文件里的任何实体，但别人不能访问。通常情况下，某个接口或 Framework 作为内部结构使用时，你可以将其设置为 internal 级别。代码中的所有实体，如果没有定义其访问级别，那么它们默认均为 internal 级别。
private。只能在当前源文件中使用的私有实体。使用 private 级别，可以用来隐藏某些功能的实现细节。
一般情况下单目标应用访问级别使用默认即可，如果有需要隐藏一些功能的实现细节可使用 private 级别，Framework 中需要开放为 API 的使用 public 级别。
**/

//3、访问控制语法
public class SomePublicClass {}
internal class SomeInternalClass {}
private class SomePrivateClass {}

public var somePublicVariable = 0
internal let someInternalConstant = 0
private func somePrivateFunction() {}

// 默认访问级别 internal
class SomeInternalClass1 {}
let someInternalConstant1 = 0

/**
4、各种类型的访问控制
（1）常量、变量、属性、下标
如果常量、变量、属性、下标索引的定义类型是 private 级别，那么它们必须要明确的声明访问级别为 private。
常量、变量、属性、下标索引的 Getters 和 Setters 的访问级别继承自它们所属成员的访问级别。
（2）元组
元组的访问级别遵循元组内部最低的访问级别。
注意：元组的访问级别是在它被使用时自动推断出的，而不是明确的声明。
（3）函数
函数的访问级别需要根据该函数的参数类型、返回类型的访问级别得出。如果根据参数类型和返回类型得出的函数访问级别不是默认 internal 级别，那么就需要明确地声明该函数的访问级别。

//如下述示例中的 private 不可省略：
//private func someFunction() -> (SomeInternalClass,SomePrivateClass){
    //function implementation gose here
    
//}

（4）枚举
枚举成员的访问级别继承自枚举，不能为枚举中的成员指定访问级别。
用于枚举定义中的任何原始值或者关联值类型必须有一个访问级别，且必须高于枚举的访问级别。
（5）类
private 类中的所有成员默认为 private 级别。internal、public 类中的所有成员默认为 internal 级别。
（6）嵌套类型
定义在 private 级别的类型中的嵌套类型默认是 private 级别。
定义在 public 或者 internal 级别的类型中的嵌套类型默认是 internal 级别。
注意：嵌套类型不被包含他的类型所束缚，依然可以在 private 级别的类型中定义 public 级别的嵌套类型。
*/
//(7）子类
//    子类的访问级别不能高于父类的访问级别。
//    子类可以通过重写父类的任意成员改变其访问级别。

public class A {
    private func someMethod(){}
}
internal class B:A {
    override internal override func someMethod() {
        
    }
}

/**
（8）初始化
public、internal 类型的默认初始化方法为 internal 级别。private 类型的默认初始化方法为 private 级别。
自定义的初始化方法访问级别必须要低于或等于它所属类的访问级别，如果是 Required 定义的初始化方法，必须和所属类的访问级别相同。
如果一个默认级别的结构体的所有存储属性访问级别均为 private 级别，那么他的默认逐一成员初始化方法为 private 级别，但是默认的初始化方法还是 internal 级别。
（9）协议
不能定义一个级别高的协议去继承一个级别低的协议。
可以定义一个级别高的类去继承一个级别低的协议，但继承了协议的类的访问级别遵循它本身和协议中最低的访问级别。比如 public 级别的类继承了 internal 协议，那么该类访问级别就是 internal。
（10）扩展
如果一个扩展采用了某个协议，你就不能对该扩展使用访问级别修饰符来声明了。该扩展中实现协议的方法都会遵循该协议的访问级别。
（11）泛型
泛型类型或泛型函数的访问级别遵循泛型类型、函数本身、泛型类型参数三者中访问级别最低的级别。
（12）类型别名
新定义的类型别名访问级别不能高于原类型的访问级别。
*/


