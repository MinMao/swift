// Playground - noun: a place where people can play

import UIKit

/**
*语法
*每个成员值通过关键字case来定义
*成员名字必须以一个大写字母开始
*多个成员值可以出现在同一行上，用逗号隔开
**/

//定义枚举
enum CompassPoint {
    case North
    case South
    case East
    case West
}

enum Planet {
    case Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune
}

var directionToHead:CompassPoint = CompassPoint.West;
directionToHead = .East; //当已经推断出变量或常量的枚举类型后，访问时可以不写枚举名

switch directionToHead {
case .North:
    println("Lots of planets have a north");
case .South:
    println("watch out for penguins");
case .East:
    println("Where the sun rises");
case .West:
    println("Where the skies are blue");
}
// prints "Where the sun rises"

//2、关联值
//swift的枚举可以定义为任何类型的关联值，如果需要的话，每个成员的数据类型可以是各不相同的

//定义了一个包含条形码和二维码的枚举
enum BarCode {
    case UPCA(Int,Int,Int,Int)
    case QRCode(String)
}
var productBarcode = BarCode.UPCA(8, 85999, 51226, 3);
productBarcode = .QRCode("ABCDEFGHIJKLMNOP");

switch productBarcode {
case .UPCA(let numberSystem,let manufaturer,let product,let check):
    println("UPC-A:\(numberSystem),\(manufaturer),\(product),\(check)");
case .QRCode(let productCode):
    println("QR code:\(productCode)")
    
}

//3、原始值(Raw Values)
/**
*枚举成员可以被默认值（称为原始值）预先提交，其中这些原始值具有相同的类型。
原始值可以是字符串，字符，或者任何整型值或浮点型值。
每个原始值在它的枚举声明中必须是唯一的。当整型值被用于原始值，如果其它枚举成员没有值时，他们会自动递增。
**/
enum Planet1:Int{
    case Mercury = 1,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune
}
let earthsOrder = Planet1.Earth.rawValue;
let possiblePlant1 = Planet1(rawValue: 7);

let positionToFind = 9;
if let somePlanet = Planet1(rawValue: positionToFind){
    switch somePlanet {
    case .Earth:
        println("Mostly harmless");
    default:
        println("Not a safe place humans");
    }
}else{
    println("There isn't a planet at position \(positionToFind)");
}


//4、枚举是值类型
