// Playground - noun: a place where people can play

import UIKit

//1、概念
/**
初始化

    初始化是为了使用某个类、结构体或枚举类型的实例而进行的准备过程。
    初始化是通过定义构造器（Initializers）来实现的。

*/
//2、存储属性的初始赋值
/**

    类和结构体在实例创建时，必须为所有的存储属性设置合适的初始值。
*/
struct Fahrenheit {
    //方式1：默认属性值
    var city = "sahnghai";
    var temperature : Double;
    //方式2：构造器
    init() {
        temperature = 32.0;
    }
}
var f = Fahrenheit();
println("\(f.city) is \(f.temperature)° Fanhrenheit");

//3、定制初始化
//1⃣️构造参数 ：你可以在定义构造器时提供构造参数，为其提供定制化构造所需值的类型和名字

struct Celsius {
    var temperatureInCelsius : Double;
    init (fromFarenheit fahrenheit : Double) {
        temperatureInCelsius = (fahrenheit - 32.0) / 1.8;
    }
    
    init (kelvin : Double) {
        temperatureInCelsius = kelvin - 273.15;
    }
    
    init (_ celsius : Double) {
        temperatureInCelsius = celsius;
    }
}

let boilingPointOfWater = Celsius(fromFarenheit:212.0);
let freezingPointOfWater = Celsius(kelvin: 273.15);
let bodyTemperature = Celsius(37.0);

//构造器并不像函数和方法那样在括号前有一个可辨识的名字，所以在调用构造器时，主要通过构造器中的参数名和类型来确定需要调用的构造器。正因为参数如此重要，Swift 默认会为每个构造器的参数自动生成一个跟内部名字相同的外部名。

//注意：如果你不希望为构造器的某个参数提供外部名字，你可以使用下划线（_）来替代。

//2⃣️可选属性类型
//如果你定制的类型包含一个逻辑上允许取值为空的存储属性，不管是因为它无法在初始化时赋值，还是因为它之后可能被赋值为空，你都需要将它定义为可选类型。可选类型的属性将自动初始化为 nil

class SurveyQuestion {
    var text: String;
    var response : String ?;
    init (text : String){
        self.text = text;
    }
    
    func ask (){
        println(text);
    }
}
let cheeseQuestion = SurveyQuestion(text: "Do you like cheese ?");

cheeseQuestion.ask();
cheeseQuestion.response = "Yes,i do like cheese!";

//3⃣️初始化过程中修改常量属性
//只要在初始化过程结束之前常量的值都能确定，你可以在任意时间点修改常量属性的值
class SurveryQuestion1 {
    let text = "";
    init (text : String) {
        self.text = text;
    }
}

let BEEtsQuestion = SurveyQuestion(text: "How about beets");

//4、默认构造器
/***

    Swift 将为所有属性已有默认值且自身没有定义任何构造器的结构体或基类，提供一个默认的构造器。
    这个默认构造器将简单地创建一个所有属性值都为默认值的实例。
    如果是结构体的默认构造器，它还是一个逐一成员构造器。

*/
//类的默认构造器
class ShoppingListItem {
    
    var name: String?;
    var quantity = 1;
    var purchased = false;
}

var item = ShoppingListItem();


//结构体的默认构造器
struct Size {
    var width = 0.0,height = 0.0;
}
let zeroByZero = Size();
let twoByTwo = Size(width: 2.0, height: 2.0);

//5、值类型的构造器代理
//构造器代理指构造代理器通过调用其他构造器来完成实例的部分初始化过程
//注意： 如果你为了某个值类型定义了一个定制的构造器，你将无法访问到默认构造器
struct Size1 {
    var width = 0.0,height = 0.0;
}

struct Point {
    var x = 0.0,y = 0.0;
}

struct Rect {
    var origin = Point();
    var size = Size1();
    
    init(){}
    
    init(origin:Point,size:Size1) {
        self.origin = origin;
        self.size = size;
    }
    
    init (center:Point,size:Size1) {
        let originX = center.x - (size.width / 2);
        let originY = center.y - (size.height / 2);
        self.init(origin:Point(x: originX, y: originY),size:size);
        self.init();
    }
}

let basicRect = Rect();
let originRect = Rect(origin: Point(x: 2.0, y: 2.0), size: Size1(width:5.0, height:5.0));
let centerRect = Rect(center: Point(x: 4.0, y: 4.0), size: Size1(width: 3.0, height: 3.0));

//6、类的继承和初始化
/***
    类里面的所有存储属性，包括所有继承自父类的属性都必须在初始化过程中设置初始值。
    Swift 提供了两种类型的类构造器来确保所有类实例中存储属性都能获得初始值：指定构造器和便利构造器。

    （1）指定构造器和便利构造器。
        指定构造器将初始化类中提供的所有属性，并根据父类链往上调用父类的构造器来实现父类的初始化。
        便利构造器调用同一个类中的指定构造器，并为其参数提供默认值。通过关键字 convenience 定义的构造器。

    （2）构造器链
        Swift 采用以下三条规则来限制构造器之间的代理调用：
        指定构造器必须调用其直接父类的的指定构造器。
        便利构造器必须调用同一类中定义的其它构造器。
        便利构造器必须最终以调用一个指定构造器结束。
        每一个类都必须拥有至少一个指定构造器；便利构造器（选择性构造）可以没有，也可以有多个。

    （3）两段式初始化过程安全检查
        Swift 编译器将执行4种有效的安全检查，以确保两段式初始化能顺利完成：
        指定构造器必须保证它所在的类引入的所有属性都初始化完成之后才能将其它构造任务向上代理给父类中的构造器。
        指定构造器必须先向上代理调用父类构造器，然后再为继承的属性设置新值。否则，指定构造器赋予的新值将被父类中的构造器所覆盖。
        便利构造器必须先代理调用同一类中的其它构造器，然后再为任意属性赋新值。否则，便利构造器赋予的新值将被同一类中其它指定构造器所覆盖。
        构造器在第一阶段构造完成之前，不能调用任何实例方法、不能读取任何实例属性的值，也不能引用 self 的值。

    （4）构造器的继承和重载
        如果你重载的构造器是一个指定构造器，你可以在子类里重载它的实现，并在自定义的构造器中调用父类的构造器。
        如果你重载的构造器是一个便利构造器，你的重载过程必须通过调用同一类中提供的其它指定构造器来实现。

    （5）自动构造器的继承
        如果子类没有定义任何指定构造器，它将自动继承所有父类的指定构造器。
        如果子类提供了所有父类指定构造器的实现，不管是通过上述规则继承过来的，还是通过自定义实现的，它将自动继承所有父类的便利构造器。

**/

//定义一个父类，提供一个指定构造器和便利构造器
class Food {
    var name : String;
    init (name : String) {
        self.name = name;
    }
    
    convenience init () {
        self.init(name : "Unnamed");
    }
}
let namedMeat = Food(name: "Bacon");
println(namedMeat.name);

let mysteryMeat = Food();
println(mysteryMeat.name);

//定义一个类 ReciperIngredient继承 Food，并提供一个指定构造器和一个便利构造器
class ReciperIngredient :Food {
    var quantity : Int;
    init(name : String, quantity : Int) {
        
        //指定构造器必须保证他所在的类引入的属性都初始化完成后才能将其他构造任务向上代理给父类中的构造器
        self.quantity = quantity;
        
        //指定构造器必须调用其直接父类的指定构造器
        super.init(name: name)
    }
    
    override convenience init(name: String) {
        //便利构造器必须调用同一类中定义的其他构造器，最终以调用一个指定构造器结束
        self.init(name:name,quantity:1);
    }
}

let oneMysteryitem = ReciperIngredient();
let oneBacon = ReciperIngredient(name: "bacon");
let sizEggs = ReciperIngredient(name: "Eggs", quantity: 6);


//定义一个类 shoppingListItem 继承 ReciperIngredient，自动继承父类的所有指定构造器和便利构造器
class ShoppingListItem1 : ReciperIngredient {
    var purchase = false;
    var description : String {
        var output = "\(quantity) x \(name)";
        output += purchase ? "Yes" : "No";
        return output;
    }
}

var breakfastList = [
    ShoppingListItem1(),
    ShoppingListItem1(name: "bacon"),
    ShoppingListItem1(name: "Eggs", quantity: 6),
];
breakfastList[0].name = "Origin juice";
breakfastList[0].purchase = true;
for item in breakfastList {
    println(item.description);
}


//7、可失败构造器
//1⃣️
//通过（init ？）来定义可失败构造器，如果构造器内返回了nil，说明构造失败，实例不存在
//注意：可失败构造器和不可失败构造器不能使用相同的参数类型和参数名

//定义一个类animal，包含一个可失败构造器
struct Animal {
    let species:String;
    init?(species: String) {
        if species.isEmpty { return nil}
        self.species = species;
    }
}

let someCreature = Animal(species: "Giraffe");
if let giraffe = someCreature {
    println("An Animal was initialized with a species of \(giraffe.species)");
}

let anonymousCreature = Animal(species: "");
if anonymousCreature == nil {
    println("The anonymous creature could not be initialized");
}

//2⃣️可失败构造器的传播
class product {
    let name : String?;
    init? (name: String) {
        if name.isEmpty {return nil}
        self.name = name;
    }
}
class CartItem: product {
    let quantity: Int?;
    init?(name: String, quantity:Int) {
        super.init(name: name);
        if quantity < 1 {return nil}
        self.quantity = quantity;
    }
}

if let twoSocks = CartItem(name: "sock", quantity: 2) {
    println("Item :\(twoSocks.name),quantity:\(twoSocks.quantity)");
}

if let zeroShirts = CartItem(name: "shirt", quantity: 0) {
    println("Item :\(zeroShirts.name) quantity:\(zeroShirts.quantity)");
}else{
    println("unable to initialized zero shirt");
}

if let oneUnnamed = CartItem(name: "", quantity: 1) {
    println("Item:\(oneUnnamed.name),quantity:\(oneUnnamed.quantity)");
}else{
    println("unable to initialized one unnamed product");
}

//3⃣️可失败的构造器重载
class Document {
    var name: String?;
    init? (name: String) {
        if name.isEmpty {return nil}
        self.name = name;
    }
}

class AotomaticallyNamedDocument: Document {
    override init() {
        super.init();
        self.name = "[Untitled]";
    }
    override init?(name: String) {
        super.init(name: name);
        if name.isEmpty {
            self.name = "[Untitled]";
        }else{
            self.name = name;
        }
    }
}

//8、强制构造器
//通过关键字 required 定义的构造器，决定了它所有的子类都必须实现这个构造器

//9、通过闭包和函数来设置属性的默认值
/*语法格式如下：
class SomeClass {
    let someProperty : SomeType = {
        return someValue;
    }()
}

*/
//注意：闭包结尾的大括号后面接了一对空的小括号
//如果你忽略了这对括号，相当于是将闭包本身作为值赋给了属性，而不是将闭包的返回值赋值给属性






