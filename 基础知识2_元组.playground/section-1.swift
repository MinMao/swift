// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*元组*/

let http404Error = (404, "Not Found")


//内容分解
let (statusCode, statusMessage) = http404Error;

print("the status code is \(statusCode) \nthe status message is \(statusMessage)")

//部分分解
let (justStatusCode,_) = http404Error;
print("\nthe status code is \(justStatusCode)")

//下标访问
print("\nthe status code is \(http404Error.0)")
print("the status message is \(http404Error.1)")

//元素命名
let newStatuscode = (status:404,message:"not found")
print(newStatuscode)

print("\nstatus code is \(newStatuscode.status)\nmessage is \(newStatuscode.message)")


let newStatusCode1 = (status:http404Error.0,message:http404Error.1);
print("\nstatus is \(newStatusCode1.status) \nmessage is \(newStatusCode1.message)")


/**
*可选
*用来处理值可能缺失的情况。一个原始形式为（type）的可选类型形式为(type?)
*/
//(1)nil
//可以给可选变量赋值为nil 来表示它没有值，在swift中，nil不是指针，他是一个确定的值，用来表示值缺失
var serverResponseCode :Int? = 404
serverResponseCode = nil

//声明一个可选常量或者变量但没有赋值，他们会自动设置为nil
var surveyAnswer: String?

//(2)if语句以及强制解析 
//可以使用if语句配合（== 或!=）来判断一个可选是否包含值
//强制解析:若果确定可选包含值时，可以在可选后面加一个感叹号(!)来获取值
let possibleNumber = "123";
let convertedNumber = Int(possibleNumber)

if convertedNumber != nil{
    print("convertedNumber is \(convertedNumber)") //Optinal(123)
    print("convertedNumber is \(convertedNumber!)")//123
}

//(3)可选绑定
//使用可选绑定来判断可选是否包含值，如果包含就把值赋给一个临时常量或者变量，调用时也不用再使用（!）来获取值。
let possibleNumber1 = "123"
if let actualNumber = Int(possibleNumber){
    print("'\(possibleNumber)' has an Integer value of \(actualNumber)")
}else{
    print("'\(possibleNumber)' coule not be converted to an Integer")
}

//(4)隐式解析可选
//有时候在程序架构中，第一次被赋值之后，可以确定一个可选总会有值。在这种情况下，每次都要判断和解析可选值是非常低效的，可将其定义为隐式解析可选。（主要用于类的初始化过程中）
//注意：如果一个变量之后可能变成 nil 的话请不要使用隐式解析可选。
let possibleString:String? = "an optinal string."
let forcedString:String = possibleString! //需要感叹号获取值

let assumedString:String! = "an implicitly unwrapped optinal string"
let implicitString:String = assumedString //不需要感叹号

if assumedString != nil{
    print(assumedString)  //print "an implicitly unwrapped optinal string"
}

if let definiteString = assumedString{
    print(definiteString) //print "an implicitly unwrapped optinal string"
}

/*
*断言
* 在某些不满足特定条件的情况下，可以通过定义断言来结束代码运行并通过调试找到值缺失的原因。
断言会在运行时判断一个逻辑条件是否为 true，如果为 true 继续运行，如果为 false 停止运行代码。
*/
let age = -3
//继续运行代码
assert(age < 0)

//停止运行代码
//assert(age >= 0, "a person's age cannot be less than zero")



