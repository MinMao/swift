// Playground - noun: a place where people can play

import UIKit

/*
 类和结构体
*/

//1、类和结构体的对比
//1⃣️共同点
/*
    定义属性用于储存值
    定义方法用于提供功能
    定义下标用于通过下标语法访问值
    定义初始化器用于生成初始化值
    通过扩展以增加默认实现的功能
    符合协议以对某类提供标准功能
*/
//类独有的特性
/*
    继承性。允许一个类继承另一个类的特征
    类型转换。允许在运行时检查和解释一个类实例的类型
    析构器。允许一个类实例释放任何其所被分配的资源
    引用计数。允许对一个类的多次引用
*/
//注意：结构体总是通过被复制的方式在代码中传递，英雌不要使用引用计数

//2、类和结构体的使用
//定义结构体
struct Resolution {
    var width = 0;
    var height = 0;
}

//定义类
class videoMode {
    var resolution = Resolution();
    var interlaced = false;
    var framerate = 0.0
    var name:String?
}
//实例
let someResolution = Resolution();
let someVedioMode = videoMode();
//访问属性
println(someResolution.width);
//访问子属性
println(someVedioMode.resolution.width);

//为属性变量赋值
someVedioMode.resolution.width = 1280;
println(someVedioMode.resolution.width);

//成员逐一初始化器（只有结构体有，类没有）
let vga = Resolution(width: 640, height: 480);


//3、结构体是值类型，类是引用类型
//结构体是值类型
let hd = Resolution(width: 1920, height: 1000);
var cinema = hd;
cinema.width = 2048;
println(hd.width);
println(cinema.width);


//类是引用类型
let tenEighty = videoMode();
tenEighty.framerate = 25.0;
let alsoTenEighty = tenEighty;
alsoTenEighty.framerate = 30.0;
println(tenEighty.framerate);
println(alsoTenEighty.framerate);

//结构体被传递的时候实际上操作的是其拷贝。而类被传递的时候引用的是已存在的实例本身。

//注意：在众多的数据类型中，只有类是引用类型，其他类型全部是值类型

//4、恒等运算符
// ===: 表示两个类型(Class Type)的常量或者变量引用同一个实例。（相对：!==）
// ==: 表示两个实例的值“相等”或"相同"。（相对：!=）
println(tenEighty === alsoTenEighty);  //true

//5、类和结构体的选择 ：由于类和结构体的类型不同，意味着他们适用的场景也不尽相同

//--按照通用的准则，当符合一条或多条以下条件时，请考虑构建结构体:
    /*
        结构体的主要目的是用来封装少量相关简单的数据值
        能够预计一个结构体实例在赋值或传递时，封装的数据将会被拷贝而不是被引用
        任何在结构体中存储的值类型属性，也将会呗拷贝，而不是被引用
        结构体不需要去继承另一个已存在的类型属性或者行为
    */

/*
    在所有其它案例中，定义一个类，生成一个它的实例，并通过引用来管理和传递。
    实际中，这意味着绝大部分的自定义数据构造都应该是类，而非结构体。
*/

