//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
enum CarEngineErrors:ErrorType{
    case NoFuel
    case OilLeak
    case LowBattery
}
class myVC: UIViewController {
    let fuelReserve = 20.0
    let oilOk = true
    let batteryReserve = 0.0
    
    func checkEngine() throws {
        guard fuelReserve > 0.0 else {
            throw CarEngineErrors.NoFuel
        }
        
        guard oilOk else {
            throw CarEngineErrors.OilLeak
        }
        
        guard batteryReserve > 0.0 else {
            throw CarEngineErrors.LowBattery
        }
        
        /*
            guard 关键词是 Swift 2.0 为了增强控制流首次引用的。当执行到 guard 语句时，首先会检查条件语句。如果条件为 false，则 else 部分会被执行
        */
        
    }
    
    func startEngine() {
        do {
            try checkEngine()
            print("engine started", terminator: "\n")
        } catch CarEngineErrors.NoFuel {
            print("No fuel !")
        } catch CarEngineErrors.OilLeak {
            print("Oil leak !")
        } catch CarEngineErrors.LowBattery {
            print("Low Batteryc !")
        } catch {
            //Default
            print("Unknown reason !")
        }
        print("Unknown reason !")
        
    }
    
    /*
        Swift 里的错误处理机制，需要使用 do-catch 语句来抓取异常并进行恰当处理。
        
        每个 catch 从句都匹配特定的错误，并且指定相关错误的响应机制。在上面的例子中，batteryReserve 变量被置为0，这种情况下调用 startEngine()将会抛出 .LowBattery 异常。
    
        假如把 batteryReserve 重置为 1.0，这样就没有异常抛出，窗口打印「Engine started」的提示信息。
    
        类似于 switch 语句，Swift 2 的错误处理机制是完备的，你需要考虑到所有可能的错误情况。所以我们需要包含一个不指定类型的 catch 从句。
    */
}

var myVc = myVC()
myVc.startEngine()


protocol aProtocol {
    func aprotocolpersentPage() ->Float
}


class Dog: aProtocol {
    var age : Int!
    func aprotocolpersentPage() -> Float {
        return 0.85
    }
}

class Cat : aProtocol {
    var age : Int!
    func aprotocolpersentPage() -> Float {
        return 0.45
    }
}

let dog = Dog()
dog.aprotocolpersentPage()

let cat = Cat()
cat.aprotocolpersentPage()



extension aProtocol {
    var aProtocolIndex:Int {
        get {
            return Int (aprotocolpersentPage() * 100)
        }
    }
}

dog.aProtocolIndex

cat.aProtocolIndex

/*
    Swift 2.0 允许开发者应用扩展到协议类型。随着协议的扩展，你可以通过添加一个特定协议，为所有类添加函数或属性，也便于扩展协议的功能。
*/


/*可用性检查*/

//检查类是否存在的方式
if NSClassFromString("NSURLQueryItem") != nil {
    //iOS 8 or up
    
} else {
    //Earlier iOS versions
}


/*
    从 Swift 2 开始支持 API 在不同版本下的可用性检查。你可以简单定义一个可用性条件，所以相应的代码块只能在特定的 iOS 版本下执行，举例如下：
*/
if #available(iOS 8,*) {
    //ios8 or up
    let queryItem = NSURLQueryItem()
} else {
    //Earlier iOS versions
    
}

//do-while  更改为 repeat-while
//eg:

var i = 0
repeat {
    i++
    print(i)
} while i < 10

func checkSomething() {
    print("CheckPoint 1")
    doSomething()
    print("CheckPoint 4")
}

func doSomething() {
    print("CheckPoint 2")
    defer {
        print("Clean up here")
    }
    print("CheckPoint 3")
}


checkSomething();
/*
    在Swift 2.0中，Apple提供了defer关键字，让我们可以实现同样的效果。
*/


extension String {
    var banana : String {
        let shortName = String(characters.dropFirst(1))  //取第一个字符后面的子字符串
        return "\(self)  ---  \(shortName)"
    }
    
    var apple : String {
        let sName = String(characters.dropLast(1))       //取最后一个字符之前的子字符串
        return "\(self)  ---  \(sName)"
    }
}

let bananaName = "Jimmy".banana
let bananaName1 = "Jimmy".apple







