// Playground - noun: a place where people can play

import Cocoa

//泛型（Generics）
//1、概念
/**
1、概念
泛型可以让你根据需求定义适用于任何类型的灵活且可重用的函数和类型。
如何命名占位类型参数？
简单情况下，泛型函数或泛型类型需要指定一个占位类型，通常用一单个字母 T 来命名类型参数。
复杂情况下，建议使用更多的描述类型参数，命名规则为大写字母开头的驼峰式命名法。
*/

//2、为什么会有泛型
//以往由于功能相同、类型不同，实现两个不同类型的数据交换功能需写两个函数。现在通过泛型写一个通用函数就能实现上述通性功能。
func swapTwoInts(inout a: Int, inout b: Int) {
    let temporaryA = a
    a = b
    b = temporaryA
}
func swapTwoStrings(inout a: String, inout b: String) {
    let temporaryA = a
    a = b
    b = temporaryA
}
//泛型函数实现通性功能
//占位符跟在函数名后面，用 <> 包含，如果有多个占位符，用（,）隔开，引用时需要在参数前加上（&）。
func swapTwoValues<T>(inout a:T, inout b:T) {
    let temporaryA = a;
    a = b;
    b = temporaryA;
}
var someInt = 3;
var anotherInt = 107;
swapTwoValues(&someInt, &anotherInt);
println("someInt = \(someInt); anotherInt = \(anotherInt)");

var someString = "Hello";
var anotherString = "World";
swapTwoValues(&someString, &anotherString);
println("someString = \(someString); anotherString = \(anotherString)");

//4、泛型类型
//1⃣️泛型类型的定义  --占位符跟在类型后面，用  <> 包含
//定义一个Int类型的栈，并实现push 和pop 功能
struct IntStack {
    var items = [Int]();
    mutating func push(item:Int) {
        items.append(item);
    }
    mutating func pop() -> Int {
        return items.removeLast();
    }
}

//通过泛型类型可以定义一个通用类型的栈
struct Stack<T> {
    var items = [T]();
    mutating func push(item:T) {
        items.append(item);
    }
    mutating func pop() ->T {
        return items.removeLast();
    }
}

var stackOfStrings = Stack<String>();
stackOfStrings.push("uno");
stackOfStrings.push("dos");
stackOfStrings.push("tres");
stackOfStrings.push("cuatro");
let fromTheTop = stackOfStrings.pop();
println(stackOfStrings.items);

//2⃣️泛型类型的扩展
//为上述 Stack 添加一个只读计算属性，如果栈为空返回 nil，如果不为空返回最后一个数据。

extension Stack {
    var topItem:T? {
        return items.isEmpty ? nil : items[items.count - 1];
    }
}

var stackOfStrings1 = Stack<String>();
stackOfStrings1.push("uno");
stackOfStrings1.push("dos");
stackOfStrings1.push("tres");
stackOfStrings1.push("cuatro");
if let topItem = stackOfStrings1.topItem {
    println("The top item on the stack is \(topItem).");
}

//类型约束
//类型约束指一个类型参数必须继承自指定类，或者遵循一个特定的协议。例如 Dictionary 的键类型必须为可哈希的。
// 泛型函数的类型约束方法
//func someFunction<T:SomeClass,U:SomeProtocol>(someT:T,someU:U) {
//    //function body gose here
//}
//上述语法中，第一个类型参数 T，必须是 SomeClass 子类的类型约束；第二个类型参数 U，必须遵循 SomeProtocol 协议的类型约束。

//查找一个字符串数组中某个字符串的位置
func findStringIndex(array:[String],valueToFind:String) ->Int? {
    for (index,value) in enumerate(array) {
        if value == valueToFind {
            return index;
        }
    }
    return nil;
}
let foundIndex = findStringIndex(["cat", "dog", "llama", "parakeet", "terrapin"], "llama");

//查找一指定类型数组中某个数据的位置
func findIndex<T:Equatable>(array:[T], valueToFind:T) ->Int? {
    for (index, value) in enumerate(array) {
        if value == valueToFind {
            return index;
        }
    }
    return nil;
}
let doubleIndex = findIndex([3.1415926,0.1,0.25], 9.3);
let stringIndex = findIndex(["Mike", "Malcolm", "Andrea"], "Andrea");
//上述例子中使用 Equatable 对类型参数 T 进行类型约束，因为不是所有类型都可以用等式符号（==）进行比较的，所以使它遵循 Equatable 协议约束后方可使用。

//关联类型 - 在定义一个协议时，有的时候通过关键字 typealias 指定关联类型是非常有用的。

// 定义一个 Container 协议
protocol Container {
    // 定义一个关联类型
    typealias ItemType
    // 要求该协议必须可以添加新的 item 到容器中
    mutating func append(item: ItemType)
    // 要求该协议必须可以计算容器内 items 的数量
    var count: Int { get }
    // 要求该协议必须可以通过索引值下标获取 item
    subscript(i: Int) -> ItemType { get }
}

// 定义一个 Int 的栈，遵循 Container 协议
struct IntStack1: Container {
    var items = [Int]()
    mutating func push(item: Int) {
        items.append(item)
    }
    mutating func pop() -> Int {
        return items.removeLast()
    }
    
    typealias ItemType = Int
    mutating func append(item: Int) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
    subscript(i: Int) -> Int {
        return items[i]
    }
}

// 定义一个通用的栈，遵循 Container 协议
struct Stack1<T>: Container {
    var items = [T]()
    mutating func push(item: T) {
        items.append(item)
    }
    mutating func pop() -> T {
        return items.removeLast()
    }
    
    mutating func append(item: T) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
    subscript(i: Int) -> T {
        return items[i]
    }
}
//述代码中 IntStack 结构体中 typealias ItemType = Int 一行如果删除仍旧正常工作，因为 Swift 通过查找 append 方法的参数类型和下标的返回类型已经自动推断出 ItemType 类型为 Int。


//7、泛型中的where语句
//where 语句使你能够要求一个关联类型遵循一个特定的协议，以及（或）那个特定的类型参数和关联类型可以是相同的。

// 检查是否两个 Container 实例包含具有相同顺序的相同元素
func allItemsMatch< C1:Container,C2:Container
    where C1.ItemType == C2.ItemType,C1.ItemType:Equatable>(someContainer:C1,anotherContainer:C2) ->Bool {
        if someContainer.count != anotherContainer.count {
            return false;
        }
        for i in 0..<someContainer.count {
            if someContainer[i] != anotherContainer[i] {
                return false;
            }
        }
        return true;
}

var stackOfStrings2 = Stack1<String>();
stackOfStrings2.push("uno");
stackOfStrings2.push("dos");
stackOfStrings2.push("tres");

var anotherStackOfStrings = Stack1<String>();
anotherStackOfStrings.push("uno");
anotherStackOfStrings.push("dos");
anotherStackOfStrings.push("tres");
if allItemsMatch(stackOfStrings2, anotherStackOfStrings) {
    println("All items match");
} else {
    println("Not all items match");
}
/**
    上述示例要求了以下内容：
    C1 必须遵循 Container 协议
    C2 必须遵循 Container 协议
    C1 的 ItemType 必须和 C2 的 ItemType 相同
    C1 的 ItemType 必须遵循 Equatable 协议
*/




