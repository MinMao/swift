// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
1、字符(characters)
*/

//初始赋值
let yenSign:Character = "￥"

//遍历字符串取字符
for character in "dog"{
    println(character)
}

//计算字符数量
println(countElements("hello"))

/**
2、字符串(String)
字符串是值类型
字符串和字符之间可通过（+）链接
*/
//初始化空字符串
var emptyString = ""
var anotherEmptyString = String()

//判断字符串是否为空
println(emptyString.isEmpty)
println(anotherEmptyString.isEmpty)

//字符串可变性
var variableString = "Horse"
variableString += " and carriage"

//字符串的大小写转换
let normal = "Could you help me,please?"
println(normal.uppercaseString) //转大写
println(normal.lowercaseString) //转小写

//注意：不能将一个字符串或字符添加到一个已存在的字符变量上，因为字符变量只包含一个字符

/**
3、比较字符串
*/
//比较字符串是否相等
let quotation = "we are a lot alike,you and I."
let sameQuotation = "we are a lot alike,you and I."
if quotation == sameQuotation{
    println("These two strings are considered equal")
}

//比较字符串前(后)缀是否相等
let remeoAndJuliet = [
    "Act 1 Scene 1: Verona,A public place",
    "Act 1 Scene 2: Capulet's mansion",
    "Act 1 Scene 3: A room in Capulet's mansion",
    "Act 1 Scene 4: A street outside Capulet's mansion",
    "Act 1 Scene 5: The Great Hall in Capulet's mansion",
    "Act 2 Scene 1: Outside Capulet's mansion",
    "Act 2 Scene 2: Capulet's orchard",
    "Act 2 Scene 3: Outside Friar Lawrence's cell",
    "Act 2 Scene 4: A street in Verona",
    "Act 2 Scene 5: Capulet's mansion",
    "Act 2 Scene 6: Friar Lawrence's cell"
]

var act1SceneCount = 0
var mansionCount = 0
var cellCount = 0
for scene in remeoAndJuliet{
    if scene.hasPrefix("Act 1 "){
        ++act1SceneCount;
    }
    
    if scene.hasSuffix("Capulet's mansion"){
        ++mansionCount
    }else if scene.hasSuffix("Lawrence's cell"){
        ++cellCount
    }
}
println("There are \(act1SceneCount) scenes in Act 1")
println("\(mansionCount) mansion scenes; \(cellCount) cell scenes")


