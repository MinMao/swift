// Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"
//Protocols_协议
//1、概念及语法
/**
    协议用于统一方法和属性的名称，而不实现任何功能，通过关键字 protocol 定义。满足协议要求的类、结构体或枚举称为遵循者。
    注意：当某个类含有父类的同时并实现了协议，应当把父类放在所有的协议之前。
*/
/*
//定义一个协议
protocol SomeProtocol {
    //protovol defination gose here
}

protocol FirstProtocol {

}
protocol AnotherProtocol {

}
//定义一个结构体实现两个协议
struct SomeStruceure:FirstProtocol, AnotherProtocol {
    //struct defination gose nere
}

class SomeSuperClass {
    
}

//定义一个类，继承一个父类，并实现两个协议
class SomeClass:SomeSuperClass, FirstProtocol, AnotherProtocol {
    //class defination gose here
}

//2、协议属性要求
//协议能够要求其遵循者必须含有一些特定名称和类型的实例属性或类属性，也能够要求属性的 settable 和 gettable，但它不要求属性是存储型属性还是计算型属性。
//协议的实例属性语法
protocol SomeProtocol1 {
    var mustBeSettable: Int { get set }
    var doseNotNeedToBeSettable: Int { get }
}
//协议类型的属性语法
protocol AnotherProtocol1 {
    class var someTypeProperty:Int { get set }
}
//注意：声明为 { get set} 的属性必须可读可写；声明为 { get } 的属性必须可读，但是如果你为它实现了 setter 方法也不会报错。
//类实现 AnotherProtocol 协议时使用 class 关键字描述类型属性，结构体或者枚举实现 AnotherProtocol 协议时使用 static 关键字描述类型属性。

//定义一个协议，一个可读的String类型的fullName属性
protocol FullyNamed {
    var fullName: String { get }
}

//定义一个结构体实现FullyNamed 协议，并将 fullName属性定义为存储属性
struct Person : FullyNamed {
    var fullName :String;
}
let john = Person(fullName: "John Appleseed");

//定义一个类实现 FullyNamed 协议，并将fullName 属性定义为计算属性
class Starship:FullyNamed {
    var perfix:String?;
    var name:String;
    var fullName:String {
        return (perfix != nil ? perfix! + " " : "") + name;
    }
    init(name:String,perfix:String? = nil) {
        self.name = name;
        self.perfix = perfix;
    }
}
var ncc1701 = Starship(name: "Enterprise", perfix: "USS");

//3、协议方法要求
//协议能够要求其遵循者必须含有一些特定的实例方法和类方法。协议方法的声明与普通方法声明相似，但它不需要大括号和方法内容。
//协议方法参数可以用可变参数，但是不能用默认参数。

//定义一个生成随机数的协议
protocol RandomNumberGenerator {
    func random() -> Double;
}
//定义一个线性同余生成器实现 RandomNumberGenerator 协议
class LinereCongruentialGenerator : RandomNumberGenerator {
    var lastRandom = 42.0;
    let m = 139968.0;
    let a = 3877.0;
    let c = 29573.0;
    func random() -> Double {
        lastRandom = ((lastRandom * a + c) % m);
        return lastRandom / m;
    }
}

let generator = LinereCongruentialGenerator();
println("Here's a random number:\(generator.random())");
//变异方法要求：类实现协议的变异方法不需要使用mutating关键字，结构体和枚举实现协议的变异方法必须使用 mutating关键字
//定义一个立方体协议，一个扩大立方体的变异方法
protocol Cube {
    mutating func enlarge();
}
//定义一个类Room实现 Cube协议一个占地面积 area
class Room:Cube {
    var area = 100;
    func enlarge() {
        self.area *= 2;
    }
}
//定义一个结构体Box 实现 Cube 协议，包含长宽高
struct Box:Cube {
    var x = 5, y = 5, z = 5;
    mutating func enlarge() {
        x *= 2;
        y *= 2;
        z *= 2;
    }
}

//4、协议构造器需求
/**
    除了被设为不可继承的 final 类，其它类在实现协议时必须使用 required 关键字描述已有的构造方法。
    如果一个非 final 类同时继承一个父类，而且实现一种协议，那么这个构造器必须同时使用 required 和 override 关键字。
*/
//协议构造器语法
protocol SomeProtocol2 {
    init();
}
class SomeSuperClass2 {
    init() {
        //initializer implementaation gose here
    }
}
class SomeSubClass2: SomeSuperClass2 {
    required override init() {
        //initializer implementation gose here
    }
}

//5、协议类型
//协议本身不实现任何功能，但你可以将它当做一个成熟类型来使用。使用协议类型的场景：
    //作为函数、方法或、造器中的参数类型或返回值类型
    //作为常量、变量、属性的类型
    //作为数组、字典或其他容器中的元素类型

//定义一个生成随机数的协议
protocol RandomNumberGenerator1 {
    func random() -> Double;
}
//定义一个线性同余生成器实现 RandomNumberFenerator
class LinearCongruentialGenerator1:RandomNumberGenerator {
    var lastRandom = 42.0;
    let m = 139968.0;
    let a = 3877.0;
    let c = 29573.0;
    func random() -> Double {
        lastRandom = ((lastRandom * a + c) % m);
        return lastRandom / m;
    }
}

//定义一个色子类
class Dice {
    let sides: Int;
    let generator:RandomNumberGenerator;
    init(sides:Int, generator: RandomNumberGenerator) {
        self.sides = sides;
        self.generator = generator;
    }
    
    func roll() ->Int {
        return Int(generator.random() * Double(sides)) + 1;
    }
}

var d6 = Dice(sides: 6, generator: LinearCongruentialGenerator1());
for _ in 1...3 {
    println("Random dice roll is \(d6.roll())");
}

//6、通过扩展补充协议声明
//定义两个协议
protocol Named {
    var name:String { get set };
}
protocol TextRepresentable {
    func asText() -> String;
}

//先实现一个协议，如果有需要可以通过扩展实现其他协议
struct Person1:Named {
    var name: String;
}
extension Person1:TextRepresentable {
    func asText() -> String {
        return "A person named \(name)";
    }
}
//当一个类型已经实现了协议中的所有需求，却没有声明时，可以通过扩展来补充协议声明
//定义了两个协议
protocol Named1 {
    var name:String { get set };
}

protocol TextRepresentable1 {
    func asText() -> String;
}

//先实现一个协议，如果有需要可以通过扩展实现其他协议
struct Person2:Named1 {
    var name:String;
    func asText() -> String {
        return "A person named \(name)";
    }
}
extension Person2:TextRepresentable1{};
//注意：即使满足了协议的所有要求，类型也不会自动转变，因此你必须为它做出明显的协议声明

//7、集合中的协议类型
//协议类型可以呗集合使用，表示集合中的元素均为协议类型。
protocol TextRepresentable2 {
    func astext() -> String;
}

//声明一个数组，数组元素要求全部符合 TextRepresentable2 协议
let things = [TextRepresentable2]();

//8、协议中的继承
//协议能够继承一到多个其他协议，并同时添加更多的要求，语法与类继承类似。
protocol InHeritingProtocol:SomeProtocol,AnotherProtocol {
    //protocol defination gose here
}

//9、类专属协议
//你可以通过关键字 class 为协议声明称类专属协议，结构体和枚举不可实现。dass关键字必须放在协议列表第一个
protocol SomeClassOnlyProtocol:class,SomeInheritedProtocol {
    //class -only protocol defination goes here
}
*/
//协议合成
//协议合成并不会生成一个新协议类型，而是将多个协议合成为一个临时的协议
protocol Named3 {
    var name: String { get };
}
protocol Aged {
    var age:Int { get };
}
struct Person3:Named3,Aged {
    var name:String;
    var age:Int;
}

func wishHappyBirthday(celebrator:protocol<Named3,Aged>) {
    println("Happy birthday \(celebrator.name) - you're \(celebrator.age)!");
}
let birthdayPerson = Person3(name: "Malcolm", age: 21);
wishHappyBirthday(birthdayPerson);

//11、检验协议的一致性
//使用 is 检验协议的一致性。使用 as 将协议类型向下转换（downCast） 为其它协议类型
/**
    is用来检查实例是否遵循了某个协议
    as?返回一个可选值，当实例遵循协议时，返回该协议类型；否则返回 nil
    as用以强制向下转换类型
    如果你想要检验协议的一致性，需要将该协议标记为 @objc。而且只能被类调用。
*/

@objc protocol HasArea {
    var area:Double { get }
}

class Circle: HasArea {
    let pi = 3.1415927;
    var radius:Double;
    var area:Double {
        return pi * radius * radius;
    }
    init(radius:Double){ self.radius = radius;}
}

class Country: HasArea {
    var area:Double;
    init(area:Double){ self.area = area;}
}

class Animal {
    var legs:Int;
    init(legs:Int) {
        self.legs = legs;
    }
}

let objects:[AnyObject] = [
    Circle(radius: 2.0),
    Country(area: 243_610),
    Animal(legs: 4)
];

for object in objects {
    if let objectWithArea = object as? HasArea {
        println("Area is \(objectWithArea.area)");
    }else{
        println("SomeThing that doesn't have an area");
    }
}

//12可选协议要求
//可选协议含有可选成员，其遵循者可以选择是否实现这些成员。在协议中使用 optional 关键字作为前缀来定义可选成员，可选协议在调用时使用可选链。
//注意：可选协议只能在含有 @objc 前缀的协议中生效。

// 定义一个可选协议
@objc protocol CounterDataSource {
    optional func incrementForCount(count: Int) -> Int
    optional var fixedIncrement: Int { get }
}
@objc class Counter {
    var count = 0
    var dataSource: CounterDataSource?
    func increment() {
        if let amount = dataSource?.incrementForCount?(count) {
            count += amount
        } else if let amount = dataSource?.fixedIncrement? {
            count += amount
        }
    }
}

// 定义一个 ThreeSource 实现上述的可选协议中的 fixedIncrement 属性
class ThreeSource: CounterDataSource {
    let fixedIncrement = 3
}
var counter = Counter()
counter.dataSource = ThreeSource()
for _ in 1...4 {
    counter.increment()
    println(counter.count)
}

// 定义一个 TowardsZeroSource 实现上述的可选协议中的 incrementForCount 方法
class TowardsZeroSource: CounterDataSource {
    func incrementForCount(count: Int) -> Int {
        if count == 0 {
            return 0
        } else if count < 0 {
            return 1
        } else {
            return -1
        }
    }
}
counter.count = -4
counter.dataSource = TowardsZeroSource()
for _ in 1...5 {
    counter.increment()
    println(counter.count)
}

//13、代理
/**
    代理是一种设计模式，它允许类或结构体将一些需要它们负责的功能委托给其他类型实例。
    代理模式可以用来响应特定的动作，或者在不知道外部数据源类型的情况下接受外部数据。
    下面以一个完整的蛇与梯子游戏来对协议与代理进行说明（如果理解起来有困难，可以先了解游戏规则，在控制流一文中有描述）：
*/

// 定义一个骰子
protocol RandomNumberGenerator3 {
    func random() -> Double
}
class LinearCongruentialGenerator3: RandomNumberGenerator3 {
    var lastRandom = 42.0
    let m = 139968.0
    let a = 3877.0
    let c = 29573.0
    func random() -> Double {
        lastRandom = ((lastRandom * a + c) % m)
        return lastRandom / m
    }
}
class Dice3 {
    let sides: Int
    let generator: RandomNumberGenerator3
    init(sides: Int, generator: RandomNumberGenerator3) {
        self.sides = sides
        self.generator = generator
    }
    func roll() -> Int {
        return Int(generator.random() * Double(sides)) + 1
    }
}
//定义一个掷骰子游戏的协议和代理
protocol DiceGame {
    var dice:Dice3 { get };
    func play();
}

protocol DiceGameDelegate {
    func gameDidStart(game:DiceGame);
    func game(game:DiceGame, didStartNewTurnWithDiceRoll diceRoll:Int);
    func gameDidEnd(game:DiceGame);
}
//定义一个蛇与梯子的游戏
class SnakesAndLadders:DiceGame {
    let finalSquare = 25;
    let dice = Dice3(sides: 6, generator: LinearCongruentialGenerator3());
    var square = 0;
    var board:[Int];
    init() {
        board = [Int](count:finalSquare + 1,repeatedValue:0);
        board[03] = +08; board[06] = +11; board[09] = +09; board[10] = +02;
        board[14] = -10; board[19] = -11; board[22] = -02; board[24] = -08;
    }
    var delegate:DiceGameDelegate?;
    func play() {
        square = 0;
        delegate?.gameDidStart(self);
        gameLoop:while square != finalSquare {
            let diceRoll = dice.roll();
            delegate?.game(self, didStartNewTurnWithDiceRoll: diceRoll);
            switch square + diceRoll {
            case finalSquare:
                break gameLoop;
            case let newSquare where newSquare > finalSquare :
                continue gameLoop;
            default:
                square += diceRoll;
                square += board[square];
            }
        }
        delegate?.gameDidEnd(self);
    }
}

//定义一个蛇与梯子的代理
class DiceGameTracker:DiceGameDelegate {
    var numberOfTurns = 0;
    func gameDidStart(game: DiceGame) {
        numberOfTurns = 0;
        if game is SnakesAndLadders {
            println("Started a new game of Snakes and Ladders");
        }
        println("The game is using a \(game.dice.sides) -sides dice");
    }
    func game(game: DiceGame, didStartNewTurnWithDiceRoll diceRoll: Int) {
        ++numberOfTurns;
        println("Rolled a \(diceRoll)");
    }
    func gameDidEnd(game: DiceGame) {
        println("The game lasted for \(numberOfTurns) turns");
    }

}
let tracker = DiceGameTracker();
let game = SnakesAndLadders();
game.delegate = tracker;
game.play();

// Started a new game of Snakes and Ladders
// The game is using a 6-sided dice
// Rolled a 3
// Rolled a 5
// Rolled a 4
// Rolled a 5
// The game lasted for 4 turns

/**
    首先，定义了一个骰子，包含骰子一个边数与一个掷骰子获取随机数的功能。
    其次，定义一个掷骰子游戏的协议和代理，DiceGame 和 DiceGameDelegate 能适用于所有掷骰子的游戏。DiceGame 协议定义了一个骰子和一个 play 功能；DiceGameDelegate 协议定义了游戏开始、游戏进行、游戏结束三个功能。
    然后，定义了一个掷骰子的蛇与梯子游戏。SnakesAndLadders 在 init 中进行了游戏设置，所以剩下的所有游戏逻辑全部放在 play 方法中。需要注意的是由于玩该游戏并不一定非要代理，所以 delegate 被定义成可选的，默认设为 nil。 也就是说蛇与梯子游戏如果没有设置代理，默认是 nil，后续如果要使用可以设置合适的代理。
    最后，定义了一个蛇与梯子游戏的代理。DiceGameDelegate 实现了游戏开始、游戏进行、游戏结束三个功能，并最终打印游戏进行的过程。
    */
