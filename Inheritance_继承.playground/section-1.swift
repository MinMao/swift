// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//1
//继承：一个类可以继承（inherit）另一个类的属性、方法和其他特性。
//当一个类继承其他类时，继承类叫子类（subclass），被继承类叫超类（或父类，superClass）。子类还可以继续呗其他类继承
//注意：在swift中，继承是区分【类】与其它类的一个基本特性

//2、基类与子类生成
//不继承其它类的类，称之为基类。
//swift中的类并不是从一个通用的基类继承而来。如果你不认为你定义的类指定一个超类的话，这个类就自动成为基类。
//定义一个通用的Vehicle
class Vehicle {
    var currentSpeed = 0.0;
    var description : String {
        return "traveling at \(currentSpeed) miles per hour";
    }
    func makeNoise() {
        //do nothing - an arbitrary vehicle doesn't necessary make a noise
    }
}
let someVehicle = Vehicle();

println("Vehicle:\(someVehicle.description)");

//定义一个子类Bicycle 继承Vehicle
class Bicycle : Vehicle {
    var hasBasket = false;
}
let bicycle = Bicycle();
bicycle.hasBasket = true;
bicycle.currentSpeed = 15.0;
println("Bicycle:\(bicycle.description)");
//注意：子类只允许修改从超类继承来的变量属性，而不能继承来的常量属性

//3、重写(Overriding)
//子类可以为继承来的实例方法、类方法、实例属性或下标提供自己定制的实现，这种行为叫做重写。
//若果要重写某个特性，你需要在重写定义的前面加上override关键字


// 1、定义一个子类 Train 继承 Vehicle ，重写方法 makeNoise()
class Train : Vehicle {
    override func makeNoise() {
        println("Choo Choo");
    }
}
let train = Train();
train.makeNoise();

// 2、定义一个子类 Car 继承 Vehicle ，重写属性 description 
class Car: Vehicle {
    var gear = 1;
    override var description : String {
        return super.description + " in gear \(gear)";
    }
}
let car = Car();
car.currentSpeed = 25.0;
car.gear = 3;
println("Car:\(car.description)");

//定义一个子类AutomaticCar继承Car，重写属性观察器
class AutomaticCar : Car {
    override var currentSpeed : Double {
        didSet {
            gear = Int(currentSpeed / 10.0) + 1;
        }
    }
}
let automatic = AutomaticCar();
automatic.currentSpeed = 35.0;
println("AutomaticCar:\(automatic.description)");

//你不可以为继承来的常量存储型属性或继承来的只读计算型属性添加属性观察器。这些属性的值是不可以被设置的。
//此外还要注意，你不可以同时提供重写的 setter 和重写的属性观察器。

//4、防止重写
//你可以通过把方法、属性或下标标记为final来防止它们被重写。
//你可以通过在类关键字class前添加final（final class），这样的类是不可被继承的。

