// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//1、函数的定义和调用
func sayHello(personName:String) ->String{
    let greeting = "Hello," + personName + "!";
    return greeting;
}
println(sayHello("MinMao"));

//2、函数参数和返回值
//五返回值的函数返回的其实是一个void类型的空元组，可以被写成(),一般情况下可忽略

//1⃣️多个输入参数
func halfOpenRangeLength(start:Int,end:Int) ->Int{
    return end - start
}
println(halfOpenRangeLength(1,10))

//2⃣️无参数
func sayHelloWorld() ->String{
    return "Hello,world"
}
println(sayHelloWorld())

//3⃣️无返回值
func sayGoodBye(personName:String){
    println("Goodbye,\(personName)")
}
sayGoodBye("MinMao")

//4⃣️多个返回值
func minMax(array: [Int]) ->(min:Int,max:Int){
    var currentMin = array[0];
    var currentMax = array[0];
    for value in array[1..<array.count]{
        if value < currentMin{
            currentMin = value;
        }else if value > currentMax{
            currentMax = value;
        }
    }
    return (currentMin,currentMax);
}
let bounds = minMax([8,-6,2,109,3,71]);
println("min is \(bounds.min) and max is \(bounds.max)");

//扩展：如果返回的元组值可能为空，可以使用可选元组
func minMaxExtend(array: [Int]) ->(min:Int,max:Int)?{
    var currentMin = array[0];
    var currentMax = array[0];
    for value in array[1..<array.count]{
        if value < currentMin{
            currentMin = value;
        }else if value > currentMax{
            currentMax = value;
        }
    }
    return (currentMin,currentMax);
}
let bounds1 = minMaxExtend([8,-6,2,109,3,71]);
println("min is \(bounds.min) and max is \(bounds.max)");

//3、函数参数名
//如果本地参数名已经是合适名称，可定义速记参数名，使用（#）作为名称前缀，说明本地参数名与外部参数名相同。
//Swift 自动为默认参数值提供一个同本地参数名一样的外部参数名（调用时不可省略），效果同（#）一样。
//在某些默认有外部参数名的情况下，如果不想使用外部参数名，可用（_）来取消外部参数名。一般推荐使用外部参数名。

//1⃣️无外部参数名，可读性差
//2⃣️外部参数名
func join2(string s1:String ,toString s2:String, withJoiner jonier:String) ->String{
    return s1 + jonier + s2;
}
println(join2(string: "hello", toString: "world", withJoiner: ","));

//3⃣️速记外部参数名
func join3(#oneString:String, #anotherString:String, #withJoiner:String) ->String{
    return oneString + withJoiner + anotherString;
}
println(join3(oneString: "hello", anotherString: "world", withJoiner: ","));

//4⃣️默认参数值
func join4(string s1:String,toString s2:String,withJoiner joiner:String = " ") ->String{
    return s1 + joiner + s2;
}
println(join4(string: "hello", toString: "world", withJoiner: ","));
println(join4(string: "hello", toString: "world"));

//5⃣️默认参数值的外部参数名
func join5(s1:String,s2:String,joiner:String = " ") ->String{
    return s1 + joiner + s2;
}
println(join5("hello", "world", joiner: ","));
println(join5("hello","world"));

//6⃣️取消默认外部参数名
func join6(s1:String, s2:String,_ joiner:String = " ") ->String{
    return s1 + joiner + s2;
}
println(join6("hello", "world", ","));
println(join6("hello", "world"));

//4、可变参数
//一个可变参数可接受零个或多个指定类型的值。在参数类型后插入(...)来编写可变参数
func arithmeticmean(numbers:Double...) ->Double{
    var total:Double = 0;
    for number in numbers{
        total += number;
    }
    return total / Double(numbers.count);
}
println(arithmeticmean(1,2,3,4,5,6,7,8,9,10));

println(arithmeticmean(3,8.25,18.75));

//注意：函数最多只可能有一个可变参数，而且它必须出现在参数列表的最后

//5⃣️常量参数和变量参数
//函数的参数默认是常量，你也可以指定一个或多个参数作为变量，从而不用在函数体内定义新的变量
func alignRight(var string:String,count:Int,pad:Character) ->String{
    let amountToPad = count - countElements(string);
    if amountToPad < 1{
        return string;
    }
    let padString = String(pad)
    for _ in 1...amountToPad{
        string = padString + string;
    }
    return string;
}
let originString = "hello";
let paddedString = alignRight(originString, 10, "_")

//注意：变量参数仅存在于函数声明的声明周期中，函数体外是不可见的


//6⃣️In-Out参数
//变量参数只能在函数体内部改变，如果在某些情况下想让函数改变参数的值，直到函数调用结束后依然保持，可以通过关键字 inout 编写参数，调用函数时在变量前添加（&）符号表明它可以被函数修改。
func swapTwoInts(inout a:Int,inout b:Int){
    let temporaryA = a;
    a = b;
    b = temporaryA;
}
var someInt = 3;
var anotherInt = 107;
swap(&someInt, &anotherInt);
println("someInt is now \(someInt), and anotherInt is now \(anotherInt)");

//注意：in-out 必须是变量。可变参数不能被标记为 inout。inout 参数不能有默认值，不能被标记为 var 或 let。

//7⃣️函数类型
//在swift中可以像任何其他类型一样的使用函数类型
//1、定义两个人函数
func addTwoInts(a:Int,b:Int) ->Int{
    return a + b;
}

func multiplyTwoInts(a:Int,b:Int) ->Int{
    return a * b;
}

//2、使用函数类型
var mathFunction:(Int,Int) ->Int = addTwoInts;
println("Result:\(mathFunction(2,3))");

mathFunction = multiplyTwoInts;
println(mathFunction(2,3));

let anotherMathFunction = addTwoInts;

//3、使用函数类型作为参数
func printMathResult(mathFunction:(Int,Int) ->Int,a:Int,b:Int){
    println("Result:\(mathFunction(a,b))");
}
printMathResult(addTwoInts, 3, 5);

//4使用函数类型作为返回值
func stepForward(input:Int) ->Int{
    return input + 1;
}

func stepBackward(input:Int) ->Int{
    return input - 1;
}
func chooseStepFunction(backwards:Bool) ->(Int) ->Int{
    return backwards ? stepBackward : stepForward
}

var currentValue = 3;
let moveNearerTozero = chooseStepFunction(currentValue > 0); //返回的是一个函数
while currentValue != 0{
    println("\(currentValue)... ");
    currentValue = moveNearerTozero(currentValue);
}
println("zero!");

//8⃣️嵌套函数
//嵌套函数默认对外界是隐藏的，但仍然可以通过他们包裹的函数调用和使用它
func chooseStepFunction1(backWards:Bool) ->(Int) ->Int{
    func stepForward(input:Int) ->Int{
        return input + 1;
    }
    func stepBackward(input:Int) ->Int{
        return input - 1;
    }
    
    return backWards ? stepBackward : stepForward;
}
var currentValue1 = -4;
let moveNearerToZero1 = chooseStepFunction1(currentValue > 0);
while currentValue1 != 0{
    println("\(currentValue1)");
    currentValue1 = moveNearerToZero1(currentValue1);
}
println("zero!");




