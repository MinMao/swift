// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//下标(Subscripts):访问对象、集合或序列的快捷方式，不需要再调用实例的特定赋值和访问方法
// 下标允许你通过在实例后面的括号中传入一个或者多个索引值来对实例进行访问和赋值，下标使用subscript关键字进行定义

//语法类似实例方法和计算型属性的混合
/*
subscript(index:Int) ->Int {  //subscript must defined within a Type
    get {
        //返回与入参匹配的Int类型的值
    }
    set(newValue) {
        //执行赋值操作
    }
}
*/
//注意 : newValue的类型必须和定义的返回值类型一样

//与只读计算型属性一样，只读下标可以直接将代码写在 subscript 中，省略 get 关键字。
struct TimesTable {
    let multiplier : Int;
    subscript(index : Int) -> Int {
        return multiplier * index;
    }
}
let threeTimesTable = TimesTable(multiplier: 3);
println("six times three is \(threeTimesTable[6])");
//下标允许任意数量的入参索引，并且每个入参类型也没有限制。 下标的返回值也可以使任意类型


//定义了一个 Matrix 结构体，将呈现一个Double 类型的二维矩阵
struct Matrix {
    let rows:Int,columns: Int;
    var grid:[Double];
    init(rows:Int,columns:Int) {
        self.rows = rows;
        self.columns = columns;
        grid = Array(count: rows * columns, repeatedValue: 0.0);
    }
    func indexIsValidForRow(row:Int,column:Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns;
    }
    subscript(row:Int,column:Int) -> Double {
        get {
            assert(indexIsValidForRow(row, column: column), "Index out of range");
            return grid[(row * column) + column];
        }
        set {
            assert(indexIsValidForRow(row, column: column), "Index out of range");
            grid[(row * column) + column] = newValue;
        }
    }
}

var matrix = Matrix(rows: 2, columns: 2);
matrix.indexIsValidForRow(1, column: 2);
matrix[0,1] = 1.5;
matrix[1,0] = 3.2;
var someValue = matrix[1,1];

//上述someValue = matrix[2,2],由于超出范围，会导致断言不通过。

//下标的重载
//一个类或结构体可以提供多个下标实现，在定义下标时通过入参类型进行区分，使用下标时会自动匹配合适的下标实现运行
struct CalSquare {
    subscript(numberInt:Int) ->Int{
        return numberInt * numberInt;
    }
    
    subscript(numberDouble:Double) ->Double {
        return numberDouble * numberDouble;
    }
}

let calSquare = CalSquare();
let squareInt = calSquare[2];
let squareDouble = calSquare[2.0];