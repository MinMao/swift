// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
/**
    可选链接 _Optional_chaining
    可选链接是一个可以请求和调用可选目标（该目标可能为nil）的属性、方法或者下标的过程


    叹号（!）和问号（?）的区别：
    叹号（!）：强制解析符号。如果解析对象为 nil 会编译出错。
    问号（?）：可选链接符号。就算解析对象为 nil 也可运行，但是返回结果为 nil。
*/

//可选链接运用
class Room {
    let name : String;
    init(name:String) {self.name = name}
}
class Address {
    var buildingName:String?;
    var buildingNumber:String?;
    var street:String?
    func buildingIdentifier() ->String? {
        if buildingName != nil {
            return buildingName;
        }else if buildingNumber != nil {
            return buildingNumber;
        }else {
            return nil;
        }
    }
}

class Residence {
    var rooms = [Room]();
    var address:Address?
    var numberOfRooms: Int {
        return rooms.count;
    }
    func printNumberOfRooms() {
        println("The number of rooms is \(numberOfRooms)");
    }
    
    subscript(i:Int) ->Room {
        get {
            return rooms[i];
        }
        set {
            rooms[i] = newValue;
        }
    }
}

class Person {
    var residence:Residence?;
}

//1、通过可选链接调用属性和设置属性
let john = Person();
if let roomCount = john.residence?.numberOfRooms {
    println("John's residence has \(roomCount) room(s)");
}else{
    println("Unable to retrieve the number of rooms");
}

let someAddress = Address();
someAddress.buildingNumber = "29";
someAddress.street = "Acacia Road";
john.residence?.address = someAddress;

//2、通过可选链接调用方法
if john.residence?.printNumberOfRooms() != nil{
    println("It was possible to print the number of rooms.");
}else{
    println("It was not possible to print the number of rooms.");
}

if (john.residence?.address = someAddress) != nil {
    println("It was possible to set the address");
}else{
    println("It was not possible to set the address");
}

//3、通过可选链接调用下标和设置值
if let firstRoomName = john.residence?[0].name{
    println("The first room name is \(firstRoomName).")
} else {
    println("Unable to retrieve the first room name.")
}
john.residence?[0] = Room(name: "BathRoom");

//可选类型 调用下标
var testScores = ["Dave":[67,87,93],"Bev":[76,72,69]];
testScores["Dave"]?[0] = 91;
testScores["Bev"]?[0]++;
testScores["Brian"]?[0] = 75;

//4、多层可选链接
if let johnsStreet = john.residence?.address?.street {
    println("John's street name is \(johnsStreet).");
}else{
    println("Unable to retrieve the address");
}

let johnsHouse = Residence();
johnsHouse.rooms.append(Room(name: "Living Roome"));
john.residence = johnsHouse;

let johnsAddress = Address();
johnsAddress.buildingName = "The Larches";
johnsAddress.street = "Laurel Street";
john.residence!.address = johnsAddress;

if let johnsStreet = john.residence?.address?.street {
    println("John's street name is \(johnsStreet)");
}else{
    println("Unable to retrieve the address");
}

//5、可选链接返回方法的值
if let buildingIdentifier = john.residence?.address?.buildingIdentifier() {
    println("John's building identifier is \(buildingIdentifier)");
}

if let beginsWiithThe = john.residence?.address?.buildingIdentifier()?.hasPrefix("The") {
    if beginsWiithThe {
        println("John's building identifier begins with \"The\".");
    }else{
        println("John's building identifier dose not begins with \"The\".");
    }
}


/*
    仔细观察上述示例，可能有人会对调用下标 john.residence?[0].name 和返回方法的值 john.residence?.address?.buildingIdentifier()?.hasPrefix("The") 中（?）的位置有疑惑：
    A、调用下标中的解释：因为可选链接的问号一般直接跟在可选表达语句的后面。
    B、返回方法的值中的解释：你想要的是 buildingIdentifier 方法的返回值，而不是方法本身。

*/