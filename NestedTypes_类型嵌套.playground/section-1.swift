// Playground - noun: a place where people can play

import Cocoa

//1、概念及应用
/***
枚举类型常被用于实现特定结构体或类的功能。同样地，也能在有多种变量类型的环境中方便地定义通用类或结构体。为了实现这种功能，Swift 允许你定义类型嵌套，可以在枚举类型、结构体和类中定义支持嵌套的类型。
要在一个类型中嵌套另一个类型，必须将嵌套的类型定义写在被嵌套类型的区域 {} 内，而且可以根据需要定义多级嵌套。

**/

//定义一副21点游戏的扑克牌，Suit 代表花色，Rank代表点数
struct BlackJackCard {
    //嵌套Suitmeiju
    enum Suit:String {
        case Spades = "黑桃",Hearts = "红桃", Diamonds = "方块", Clubs = "梅花";
    }
    // 嵌套 Rank 枚举
    enum Rank:Int {
        case Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten;
        case Jack, Queen, King, Ace;
        //嵌套Values 结构体
        struct Values {
            let first:Int, second:Int?;
        }
        
        var values:Values {
            switch self {
            case .Ace:
                return Values(first: 1, second: 11);
            case .Jack:
                return Values(first: 10, second: nil);
            default:
                return Values(first: self.rawValue, second: nil);
            }
        }
    }
    
    //21点游戏的属性及方法
    let rank:Rank,suit:Suit;
    var description:String {
        var output = "suit is \(suit.rawValue)";
        output += " values is \(rank.values.first)";
        if let second = rank.values.second {
            output += " or \(second)";
        }
        return output;
    }
}

let theAceOfSpades = BlackJackCard(rank: .Ace, suit: .Spades);
println("theAceOfSpades：\(theAceOfSpades.description)");
//嵌套类型引用
println(BlackJackCard.Suit.Hearts.rawValue);