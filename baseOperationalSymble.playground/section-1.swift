// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
/*
基本运算符
*/
//1、赋值运算符
let m = 10
var n = 5
n = m
let (x,y) = (1,2)
//2、数值运算符
println(1+2)
println(2-1)
println(2*3)
println(10.0/2.5)
println("hello," + "world")
//3、求余运算符
println(9 % 4)
println(-9 % 4)
println(8 % 2.5)
//4、自增运算符
var a1 = 0
let b1 = ++a1
println(a1)
println(b1)

var a2 = 0;
let b2 = a2++
println(a2)
println(b2)
//5、单目负号
let three = 3
let minusThree = -three
let plusThree = -minusThree
//6、单目正号
let minusSix = -6
let alsoMinusSix = +minusSix
//7、复合赋值
var a = 1
a = a + 2
a += 2

//8、比较运算符
println(1 == 1)
println(2 != 1)
println(2 > 1)
println(1 < 2)
println(1 >= 1)
println(2 <= 1)
//9、三目条件运算符
let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20)
//10、空值合并运算符 （??，可以理解为 a != nil ? a! : b）
let defaultColorname = "red"
var userDefinedColorName: String?
println(userDefinedColorName ?? defaultColorname)  // print "red"

userDefinedColorName = "green"
println(userDefinedColorName ?? defaultColorname)  //print "green"
//11、闭区间运算符
for index in 1...5{
    println("\(index) times 5 is \(index * 5)")
}
//print is :
//1 times 5 is 5
//2 times 5 is 10
//3 times 5 is 15
//4 times 5 is 20
//5 times 5 is 25
//12、半闭区间运算符
let names = ["Jack", "Tom", "Ketty", "Mary", "Antony"]
let count = names.count
for i in 0..<count{
    println("Person \(i+1) is called \(names[i])")
}
//print is :
//Person 1 is called Jack
//Person 2 is called Tom
//Person 3 is called Ketty
//Person 4 is called Mary
//Person 5 is called Antony
//13、逻辑运算
let allowedEntry = false
if !allowedEntry{
    println("ACCESS DENIED")
}
let enteredDoorCode = true
let passedRetinaScan = false
if enteredDoorCode && passedRetinaScan{
    println("welcome")
}else{
    println("ACCESS DENIED")
}

let hasDoorKey = false
let knowsOverridePassword = true
if hasDoorKey || knowsOverridePassword {// 逻辑或
    println("Welcome!")
} else {
    println("ACCESS DENIED")
}


if enteredDoorCode && passedRetinaScan || hasDoorKey || knowsOverridePassword {// 组合逻辑
    println("Welcome!")
} else {
    println("ACCESS DENIED")
}