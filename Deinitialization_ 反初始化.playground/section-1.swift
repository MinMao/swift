// Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"
//1、
/***
    反初始化：在一个类实例被释放之前，会立即调用反初始化函数
    注意：反初始化只适用于 类类型
**/

//2、原理及语法
/*
    Swift 通过 ARC 自动管理内存，释放不再被引用的实例所占的内存资源，但是如果你想在资源被释放之前做一些额外的操作，可以通过调用反初始化函数来实现。（比如，创建一个类打开一个文件并写入一些数据，你需要在这个类实例被释放之前关闭该文件）
    语法：反初始化函数通过关键字 deinit 定义，不带任何参数，不带小括号。每个类最多只能有一个反初始化函数，而且不能被主动调用。

*/

//定义一个管理硬币的bank类 （该游戏中只会有一个实例）
struct Bank {
    //硬币总量
    static var coinsInBank = 10_000;
    //硬币出售，如果剩余硬币数不够需求，则将剩余的全部出售
    static func vendCoins(var numberofCoinsToVend:Int) ->Int{
        numberofCoinsToVend = min(numberofCoinsToVend, coinsInBank);
        coinsInBank -= numberofCoinsToVend;
        return numberofCoinsToVend;
    }
    
    //硬币收集
    static func receiveCoins(coins:Int) {
        coinsInBank += coins;
    }
}

//定义玩家Player类（该游戏中可有多个）
class Player {
    //拥有的硬币财产
    var coinsInPurse : Int;
    init(coins:Int) {
        coinsInPurse = Bank.vendCoins(coins);
    }
    
    //获取硬币
    func winCoins(coins:Int) {
        coinsInPurse += Bank.vendCoins(coins);
    }
    
    deinit {
        Bank.receiveCoins(coinsInPurse);
    }
}

var playerOne:Player? = Player(coins: 100);
println("A new player has joined the game with \(playerOne!.coinsInPurse) coins");
println("There are now \(Bank.coinsInBank) coins left in bank");

playerOne!.winCoins(2_000);
println("PlayerOne win 2000 coins & now has \(playerOne!.coinsInPurse) coins");
println("the Bank now only has \(Bank.coinsInBank) coins left");

playerOne = nil;
println("PlayerOne has left the gane");
println("The bank now has \(Bank.coinsInBank) coins");

//如果一个子类继承了一个父类，实例释放的时候会按顺序先自动调用子类的反初始化函数，然后调用父类的反初始化函数，而且是必须调用
class A {
    deinit {
        println("deinit has been call : A");
    }
}
class B: A {
    deinit {
        println("deinit has been call :B");
    }
}

var someB:B? = B();
someB = nil;


