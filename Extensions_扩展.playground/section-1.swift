// Playground - noun: a place where people can play

import Cocoa

//扩展(Extebsions)
//1、概念及语法
/***
    使用关键字 extension 为一个已有类型添加新功能或者使一个已有类型能够适配一个或多个协议，称为扩展。（包括没有权限获取原始源代码的情况）
    如果你定义了一个扩展向一个已有类型添加新功能，那么这个新功能对该类型的所有已有实例都是可用的，即使它们是在你的这个扩展的前面定义的。

**/

/*
eg:
    //为一个已有类型扩展新功能
    extension SomeType {
        //new functionality to add SomeType goes here
    }
    //使用扩展为 SomeType适配SomeProtocol， AnotherProtocol 协议
    extension SomeType:SomeProtocol, AnotherProtocol {
        //implementation of protocol requirements gose here
    }
*/

//2、扩展的引用
/***
    Swift 中可以扩展以下内容：
        添加新的计算属性和计算静态属性
        添加新的构造器
        添加新的实例方法和类型方法
        添加新的下标
        添加新的嵌套类型
        使一个已有类型符合某个协议
**/

//1、使用扩展添加计算属性
extension Double {
    var km:Double { return self * 1_000.0}
    var m :Double { return self}
    var cm:Double { return self / 100.0}
    var mm:Double { return self / 1_000.0}
}
let oneInch = 25.4.mm;
print("One inch is \(oneInch) meter");

//2、使用扩展添加构造器
struct Student {
    var name:String;
    var age :Int = 0;
    init(newName:String, newAge:Int) {
        self.name = newName;
        self.age = newAge;
    }
}

extension Student {
    init(newName:String) {
        self.name = newName;
    }
}

let studentOne = Student(newName: "John", newAge: 18);
let studentTwo = Student(newName: "Jack");

//3、使用扩展添加方法
extension Int {
    func repetitions(task: () -> ()) {
        for i in 0..<self {
            task();
        }
    }
}
2.repetitions({
    println("Hello !");
});

2.repetitions{ //尾随闭包方式
    println("Goodbye !");
};

//使用扩展添加的变异实例方法也可以修改改实例本身
extension Int {
    mutating func square() {
        self = self * self ;
    }
}
var someInt = 3;
someInt.square();

//4、使用扩展添加下标
extension Int {
    subscript(var digitIndex : Int) ->Int {
        var decimalBase = 1;
        while digitIndex > 0 {
            decimalBase *= 10;
            --digitIndex;
        }
        return (self / decimalBase) % 10;
    }
}
println(12345[1]);

println(12345[8]);

//5、使用扩展添加嵌套类型
extension Int {
    enum kind {
        case Negative, Zero, Positive;
    }
    var kind:kind {
        switch self {
        case 0:
            return .Zero;
        case let x where x > 0:
            return .Positive;
        default:
            return .Negative;
            
        }
    }
}

let numbers = [3,19, -27, 0, -6, 0, 7];
for number in numbers {
    switch number.kind {
    case .Negative:
        print("负数");
    case .Zero:
        print("零");
    case .Positive:
        print("正数");
    }
}
//注意：扩展可以添加新的计算属性，但是不可以添加存储属性，也不可以向已有属性添加属性观察器。
