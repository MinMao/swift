// Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

/**
类型转换(Type Casting)
    类型转换通过（is）和（as）实现
    1⃣️ is 用来检查一个实例是否属于某个特定的子类型
    2⃣️ as 包含两种向下转型的方式
*/
/*
    as?：当你不确定下转是否可以成功时使用。可选形式的类型转换总是返回一个可选值，若下转是不可能的，返回值将是 nil 。这使你能够检查下转是否成功。
    as：只有确定下转一定会成功时，才使用强制形式。当你试图下转为一个不正确的类型时，强制类型转换会导致编译出错。
*/

class MediaItem {
    var name:String;
    init(name:String) {
        self.name = name;
    }
}

class Movie: MediaItem {
    var director:String;
    init(name:String, director:String) {
        self.director = director;
        super.init(name: name);
    }
}

class Song: MediaItem {
    var artist:String;
    init(name: String, artist:String) {
        self.artist = artist;
        super.init(name: name);
    }
}

let library = [
    Movie(name: "指环王", director: "科幻"),
    Song(name: "勇气", artist: "轻松"),
    Movie(name: "里约大冒险", director: "动画"),
    Song(name: "最炫民族风", artist: "草原风"),
    Song(name: "十年", artist: "FriendShip")
];

//1、检查类型
var movieCount = 0;
var songCount = 0;
for item in library {
    if item is Movie {
        ++movieCount;
    }else if item is Song {
        ++songCount;
    }
}

println("media library contains \(movieCount) movies and \(songCount) songs");

//2、向下转型
for item in library {
    if let movie = item as? Movie {
        println("Movie:'\(movie.name)',dir: \(movie.director)");
    }else if let song = item as? Song {
        println("Song:'\(song.name)', by \(song.artist)");
    }
}
//注意：转换没有真的改变实例或它的值。潜在的根本的实例保持不变；只是简单地把它作为它被转换成的类来使用。



/**
    AnyObject 和Any
    swift为不确定类型提供了两种特殊类型别名

    AnyObject：可以代表任何class类型的实例
    Any：      可以代表除了方法类型的任何类型

*/
//1⃣️AnyObject类型
let someObjects:[AnyObject] = [
    Movie(name: "2001:A Space Odyssey", director: "Stanley Kubrick"),
    Movie(name: "Moon", director: "Duncan Jones"),
    Movie(name: "Alien", director: "Ridley Scott"),
];

for object in someObjects {
    let movie = object as Movie;
    println("Movie:'\(movie.name)',dir:\(movie.director)");
}

//简写形式
for movie in someObjects as [Movie] {
    println("Movie:'\(movie.name)',dir:\(movie.director)");
}

//2⃣️Any类型
var things = [Any]();
things.append(0);
things.append(0.0);
things.append(42);
things.append(3.14159);
things.append("hello");
things.append((3.0,5.0));
things.append(Movie(name: "Ghostbuster", director: "Ivan Reitman"));
things.append({(name:String) ->String in "Hello,\(name)"});

for thing in things {
    switch thing {
    case 0 as Int :
        println("zero as an Int");
    case 0 as Double:
        println("zero as an Double");
    case let someInt as Int:
        println("an integer value of \(someInt)")
    case let someDouble as Double where someDouble > 0:
        println("a positive double value of \(someDouble)")
    case is Double:
        println("some other double value that I don't want to print")
    case let someString as String:
        println("a string value of \"\(someString)\"")
    case let (x, y) as (Double, Double):
        println("an (x, y) point at \(x), \(y)")
    case let movie as Movie:
        println("a movie called '\(movie.name)', dir. \(movie.director)")
    case let stringConverter as String -> String:
        println(stringConverter("Michael"))
    default:
        println("something else")
    }
}




