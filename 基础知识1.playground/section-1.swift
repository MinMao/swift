// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
print(str);

var myVariable = 42;

myVariable = 60;

let myConstant = 42;

let imlicitInteger = 70;
let imlicitDouble = 70.0;
let explicitDouble : Double = 70;

let imlicitFloat = 4.0;
let explicitFloat : Float = 4;

let label = "the width is";
let width = 94;
let widthLabel = label + " " + String(width);


let 你好 = "你好世界";

let 🐶🐮 = "dogcow"


var friendlyWelcome = "Hello!"
friendlyWelcome = "Bonjour!"
// friendlyWelcome is now "Bonjour!"

let languageName = "Swift"
//languageName = "Swift++"   //常量的值不能被修改
// this is a compile-time error - languageName cannot be changed


print(friendlyWelcome);
print("this is a swift program")

print("the current value of fridenlyWelcome is \(friendlyWelcome)")

let cat = "🐴"
print(cat);  //prints "🐴“


/**
Integers   -----signed(正数，0)  && UnSigned(正数，0，负数)

Int8,Int16,Int32,Int64
***/


let minValueUInt8 = UInt8.min; //min Value is equal to 0,and is of type UInt8
let maxValueUInt8 = UInt8.max; //max Value is equal to 255,and is of type UInt8

let minValueInt8 = Int8.min; //-128
let maxValueInt8 = Int8.max; //128-1 = 127

let minValueInt16 = Int16.min //
let maxValueInt16 = Int16.max //


/**
Int   On a 32-bit platform,Int is the same size as Int32

UInt  On a 32-bit platform,Int is the same size as UInt32


Dounle represents a 64-bit floating-point number

Float represents a 32-bit floating-point number
***/


let meaningOfLife = 42 //meaningOfLife is inferred to be of type Int
let pi = 3.1415926 //pi is inferred to be of type Double



//Swift always chooses Double (rather than Float) when inferring the type of floating-point numbers
//if you combine integer and floating-point literals in an expression,a type of Double will be inferred from the context:

let anotherPi = 3 + 0.1415926 //anotherPi is also inferred to be of type Double



//Numeric Literals
let decimalInteger = 17
let binaryInteger = 0b10001   //17 in binary notation          二进制
let octalInteger = 0o21       //17 in octal notation           8进制
let hexadecimalInteger = 0x11 //17 in hexadecimal notation     16进制

let decimalDouble = 12.1875
let exponentDouble = 1.21875e1
let hexadecimalDouble = 0xC.3p0



let paddedDouble = 000123.456
let oneMillion = 1_000_000
let justOverOneMillion = 1_000_000.000_000_1



/***
Integer Conversion  ---转化

*/
//UInt8 can not store negative bumbers,and so this will report an error
//let cannotBeNegative:UInt8 = -1

//Int8 can not store a number larger than its maximum value
//and so this will also report an error
//let toooBig:Int8 = Int8.max + 1;


let twoThousand:UInt16 = 2_000
let one:UInt8 = 1;

let twoTHoussandAndOne = twoThousand + UInt16(one)


/**

Integer and Floating-point Conversion
*/

let three = 3
let pointOneFourOneFiveNineTwoSix = 0.1415926
let pi_1 = Double(three)+pointOneFourOneFiveNineTwoSix

//IntegerPi equal to 3,and is inferred to be of type Int
let integerPi = Int(pi_1)


/*
Type Aliases 类型别名
*/
typealias AudioSample = UInt16  //AudioSample是定义的又名  代表UInt16

var maxAmplitudeFound = AudioSample.min  //maxAmplitudeFound now is 0


/**
Booleans
*/

let orangeAreOrange = true
let turnipsAreDelicious = false

if turnipsAreDelicious{
    print("Mmm, tasty turnips!")
}else{
    print("Eww,turnips are horrible")
}


//let i = 1
//if i {
    //this example will not compile ,and will report an error
//}

let i = 1
if i == 1{
    //this example will compile successfully
}

/**
Tuples  元组
**/
//http404Error is of type (Int,String),and equal is (404,"Not Found")
let http404Error = (404,"Not Found")


var a = "234"
var b = Int(a)

var c: Int? = 1
c = nil

print(b)
print(b)
print(c)
print(b!)


/*匹配元组中的值*/
func fizzBuzz(number : Int) ->String{
    switch(number % 3, number%5){
    case (0, 0) :
        //number既可以被3整除，也可以被5整除
        return "FizzBuzz"
    case (0, _) :
        //number能被3整除
        return "Fizz"
    case (_, 0) :
        //number能被5整除
        return "Buzz"
    case (_, _) :
        //number即不能被3整除也不能被5整除
        return "\(number)"
    }
}
print("--------------------------------")
for i in 1...100{
    print(fizzBuzz(i))
}
print("--------------------------------")

fizzBuzz(15)



