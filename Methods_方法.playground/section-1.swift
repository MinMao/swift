// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//方法(Methods) -- 是与特定类型相关联的函数

//1、实例方法 -属于特定类、结构体或者枚举类型实例的方法。【语法与函数一致】
//注意：实例方法不能脱离于现存的实例而被调用
class Counter {
    var count = 0;
    func increment() {
        count++;
    }
    func incrementBy(amount:Int){
        count += amount;
    }
    func reset() {
        count = 0;
    }
    
}
let counter = Counter();
counter.increment();
counter.incrementBy(5);
counter.reset();

//2、方法的参数名
class Counter1 {
    var count = 0;
    func incrementBy(amount:Int,numberOfTimes:Int) {
        count += amount * numberOfTimes;
    }
}

let counter1 = Counter1();
counter1.incrementBy(5, numberOfTimes: 5);

//3、变异方法
//一般情况下，值类型的属性不能在它的实例方法中呗修改。但是可以通过变异方法（通过关键字mutating声明方法），从方法内部改变实例属性的值。
struct Point {
    var x = 0.0,y = 0.0;
    mutating func moveByX(deltaX:Double,y deltaY:Double) {
        x += deltaX;
        y += deltaY;
    }
}
var somePoint = Point(x: 1.0, y: 1.0);
somePoint.moveByX(2.0, y: 3.0);

println("the point is now at (\(somePoint.x),\(somePoint.y))");
//不能在结构体类型常量上调用变异方法，因为常量的属性不能被改变，即使想改变的时常量的变量属性页不行。

//4、self属性 - 类型的每一个实例都有一个隐含属性叫做self，self完全等同于该实例本身
enum TriStateSwitch {
    case Off,Low,High;
    mutating func next() {
        switch self {
        case Off:
            self = Low;
        case Low:
            self = High;
        case High:
            self = Off;
        }
    }
}

var ovenLight = TriStateSwitch.Low;
ovenLight.next();
ovenLight.next();

//5、类型方法 -- 类型本身调用的方法即类型方法
//声明类的类型方法，在方法的func关键字之前加上关键字class。
//声明结构体和枚举的类型方法，在方法的func关键字之前加上关键字static
struct Rectangle {
    static var color : String = "red";
    static var length = 0;
    static var width = 0;
    static func calPerimeter() ->Int{
        return (length + width) * 2;
    }
    static func describe() ->String{
        return "the Rectangle's perimeter is \(calPerimeter()) and color is \(color)";
    }
}

Rectangle.describe();

//总结： -- 一个类型方法（类、结构体、枚举）可以调用本类型中另一个类型方法的名称，而无需在方法名前面加上类型名称的前缀。结构体和枚举的类型方法也能够直接通过静态属性的名称访问，而不需要加类型名称前缀。

